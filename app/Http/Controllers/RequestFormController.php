<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;

class RequestFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(session()->has('requestform')){
            //dd(session('requestform'));

            $units = [];

            foreach (session('requestform') as $id => $value) {
                
                $unit = Unit::find($id);

                $unit->quantity = session("requestform.$id");

                $units[] = $unit;

            }

            return view('requestforms.index')
                ->with('units', $units);

        } else {
        return view('requestforms.index');

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = $id;

        $request->session()->put("requestform.$id", $unit);
        //dd($unit);

        return redirect( route('requestform.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session()->forget("requestform.$id");

      if(count(session()->get('requestform')) === 0) {

        session()->forget('requestform');
      }
      
      return back()->with('message', "Asset has been successfully removed from the form");
    }

    public function clear()
    {
        session()->forget('requestform');

        return back();
    }
}
