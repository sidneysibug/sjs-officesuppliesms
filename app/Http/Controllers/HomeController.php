<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Status;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $transactions = Transaction::all();

        return view('home')
            ->with('transactions', Transaction::all())
            ->with('statuses', Status::all());
          
            
            
    }
}
