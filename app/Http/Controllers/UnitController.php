<?php
 
namespace App\Http\Controllers;

use App\Unit;
use App\Category;
use App\Status;
use App\Availability;
use App\Transaction;
use Illuminate\Http\Request;
use Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Unit $unit)
    {

        $this->authorize('viewAny', $unit);
        $units = Unit::all()->sortBy('name');
        $availabilities = Availability::all();
        $categories = Category::all();
        

        return view('units.index')
            ->with('units', $units)
            ->with('availabilities', $availabilities)
            ->with('unit', $unit);
        
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        
        $this->authorize('create', Unit::class);
        $unit_code = strtoupper(Str::random(6));

        return view('units.create')
            ->with('categories', Category::all())
            ->with('unit_code', $unit_code);
           
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->authorize('create',Unit::class);
        $categories = Category::all();

        $validatedData = $request->validate([
           'image' => 'required|image',
           'particulars' => 'required|string',
           'uom' => 'required|string',
            'category_id' => 'required',
            'unit_code' => 'required'
            
        ]);


        ////////////experimental code/////////
        
        // $unit = new Unit($validatedData);
        // $path = $request->file('image');
        // $path_name = time().".".$path->getClientOriginalExtension();
        // $destination = "image/";
        // $path->move($destination, $path_name);
        // $unit->image = $destination.$path_name;


       
        //////////////////////////////////

        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('/images/units'),$imageName);


        // // $path = $request->file('image')->store('public/units');

        // // $url = (Storage::url($path));
        // //dd($url);

        $unit = new Unit($validatedData);
        //dd($unit);

        $unit->image = $imageName; 

        $cat_id = $unit->category_id;
        //dd($cat_id);

        $unit->unit_code = $cat_id . $request->unit_code;
        //dd($unit->unit_code);

        // // $unit->image = $url;
        // //dd($unit->unit_code);

        $unit->save();
        // //dd($unit);

        //////////////////////////

        //View::share('unit_code', $unit->unit_code);

        // return redirect( route('units.index'))
        //     ->with('unit', $unit);
            //->with('message', "Unit {$unit->name} is added successfully");
            //->with('unit_code', $unit_code );

        // return redirect(route('categories.show',$cat_id));

         return back()
            ->with('message', "Asset code '{$unit->unit_code}' with particulars '{$unit->particulars}' has been added successfully");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Unit $unit)
    {
        $this->authorize('view', $unit);
        $availabilities = Availability::all();
        //dd($asset);

        return view('units.show')
            ->with('unit', $unit)
            ->with('availabilities', $availabilities);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Unit $unit)
    {
        //
        $this->authorize('update', $unit);
    
        return view('units.edit')
            ->with('unit', $unit)
            ->with('categories', Category::all())
            ->with('availabilities', Availability::all())
            ->with('unit->unit_code',$unit->unit_code);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        $this->authorize('update', $unit);
        $validatedData = $request->validate([
            'image' => 'image',
            'availability_id' => 'required'
        ]);

        $unit->update($validatedData);

        // $path = $request->file('image');

        // if($request->hasFile('image')){
        //     $path = $request->file('image')->store('public/units');
        //     $url = (Storage::url($path));
        //     $unit->image = $url;
        // }

        if($request->hasFile('image')){
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/images/units'),$imageName);
            $unit->image = $imageName; 
        }

        

        // if($path != ""){

        //     $imageName = time().'.'.$request->image->getClientOriginalExtension();
        //     $request->image->move(public_path('/images/units'),$imageName);

        // }

        // $cat_id = $unit->category_id;
        // //dd($cat_id);

        // $unit->unit_code = $cat_id . $request->unit_code;
        // //dd($unit->unit_code);

        $unit->unit_code = $request->unit_code;
        //dd($unit_code);

        $unit->save();
        //dd($unit);

        return back()->with('message', "Asset code '{$unit->unit_code}' with particulars '{$unit->particulars}' has been updated successfully");
      

         // return redirect( route('units.index') );
          // ->with('message', "Unit {$unit->name} is updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        //
        $this->authorize('delete',$unit);
        $unit->delete();
        return redirect( route('units.index'))
            ->with('message', "Asset code {$unit->unit_code} has been deleted successfully");

    }


}
