<?php

namespace App\Http\Controllers; 

use App\Category;
use App\Unit;
use Str;
use App\Availability;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $this->authorize('viewAny', Category::class);
        $categories = Category::paginate(10)->sortBy('name');
        $units = Unit::all();
  
        return view('categories.index')
            ->with('categories', $categories)
            ->with('category', $category)
            ->with('units', $units)
            ->with('availabilities', Availability::all());
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $this->authorize('create', Category::class);
        $categories = Category::all();
        $category = Category::find($category->id);

        return view('categories.index')
            ->with('categories', $categories)
            ->with('category', $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->authorize('create', Category::class);
        $validatedData = $request->validate([
            'name' => 'required|unique:categories,name',
        ]);


        $category = new Category($validatedData);
        $category->save();

        return redirect( route('categories.index'))
            ->with('message', "Category '{$category->name}' is added successfully");

        // return back()->with('message', "Category '{$category->name}' is added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->authorize('view', $category);
        $units = Unit::all();
        $categories = Category::all()->sortBy('name');
        $unit_code = strtoupper(Str::random(6));


        return view('categories.show')
            ->with('category', $category)
            ->with('categories', $categories)
            ->with('units', $units)
            ->with('unit_code', $unit_code)
            ->with('availabilities', Availability::all());

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('update', $category);
        return view('categories.edit')
            ->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('update', $category);
        $validatedData = $request->validate([
            'name' => 'required|unique:categories,name'
        ]);
        $category->update($validatedData);
        $category->save();

        // return redirect(route('categories.show', $category->id))
        //     ->with('message', "{$category->name} is updated successfully");
        return back()->with('message', "Category '{$category->name}' is updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);
        $category->delete();

        return redirect(route('categories.index'))
            ->with('message',"Category {$category->name} has been deleted");
    }
}
