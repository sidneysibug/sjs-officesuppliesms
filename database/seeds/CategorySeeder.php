<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => 'Laptop'

        ]);

        DB::table('categories')->insert([
        	'name' => 'Printer',
        ]);

        DB::table('categories')->insert([
        	'name' => 'Ink'
 
        ]);

        DB::table('categories')->insert([
            'name' => 'Whiteboard'

        ]);

        DB::table('categories')->insert([
            'name' => 'Scissors',
        ]);

        DB::table('categories')->insert([
            'name' => 'Screen Protector'
 
        ]);

        DB::table('categories')->insert([
            'name' => 'Tape Dispenser'

        ]);

        DB::table('categories')->insert([
            'name' => 'Pencil Sharpener',
        ]);

        DB::table('categories')->insert([
            'name' => 'Stapler'
 
        ]);

        DB::table('categories')->insert([
            'name' => 'Speaker'

        ]);

        DB::table('categories')->insert([
            'name' => 'Computer Accessories',
        ]);

        DB::table('categories')->insert([
            'name' => 'Projector'
 
        ]);
    }
}
