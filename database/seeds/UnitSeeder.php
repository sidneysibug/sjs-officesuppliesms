<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('units')->insert([
            'image' => 'https://via.placeholder.com/150',
            'unit_code' => '1abc12',
            'particulars' => 'paper',
            'uom' => 'box',
            'category_id' => 1,
            'availability_id' => 1
           
        ]);

         DB::table('units')->insert([
            'image' => 'https://via.placeholder.com/150',
            'unit_code' => '2abc13',
            'particulars' => 'Gtech',
            'uom' => 'pc',
            'category_id' => 1,
            'availability_id' => 2
            

        ]);

        DB::table('units')->insert([
            'image' => 'https://via.placeholder.com/150',
            'unit_code' => '3abc14',
            'particulars' => 'Gtech',
            'uom' => 'pc',
            'category_id' => 2,
            'availability_id' => 3

        ]);

        DB::table('units')->insert([
            'image' => 'https://via.placeholder.com/150',
            'unit_code' => '1abc16',
            'particulars' => 'paper',
            'uom' => 'pc',
            'category_id' => 3,
            'availability_id' => 1
            
        ]);
    }

}
