@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Welcome!') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in.') }}
                </div>
            </div>
        </div>
    </div>

@can('isAdmin')
   
    <div class="row mt-3">
        <div class="col-12 col-md-6">
            <div class="card w-100">
              <div class="card-body">
                <h5 class="card-title">Pending Requests</h5>
                
                <p class="card-text">
                    Number of requests for admin's approval: 
                    {{ collect($transactions)->where('status_id',1)->count()}}
                </p>
                <a href="{{ route('transactions.index')}}" class="btn btn-primary">View & Update</a>
              </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card w-100">
              <div class="card-body">
                <h5 class="card-title">Appoved Requests</h5>
                
                <p class="card-text">
                    Awaiting distribution of the requested assets: 
                    {{ collect($transactions)->where('status_id',2)->count()}}
                </p>
                <a href="{{ route('transactions.index')}}" class="btn btn-primary">View Assets Requested</a>
              </div>
            </div>
        </div>
    </div>
    
@endcan

</div>
@endsection
