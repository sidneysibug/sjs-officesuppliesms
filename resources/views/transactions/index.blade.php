@if(Auth::user())
@extends('layouts.app')

@section('content')

	<div class="container">


		<div class="row">
			<div class="col-12 col-md-6 mt-4 mb-2">
				<h6>		
					<div class="input-group mb-3">
						<div class="input-group-text">
							<a 
								href="{{ route('transactions.index') }}" 
								class="mr-3 d-inline-block
								{{ Route::CurrentRouteNamed('transactions.index') ? "active1" : "" }}
								">
									All requests	
							</a>
						</div>
						
						<div class="input-group-prepend ml-3">
						    <label class="input-group-text" 
						    	for="inputGroupSelect01">
						    	Search
						    </label>
						    <input id="myInput" type="text" placeholder="...">
						 </div>

					</div>
				</h6>
				{{-- @include('transactions.partials.header') --}}
				
			</div>
		</div>
		
		<hr>

		@includeWhen(Session::has('message'),'partials.alert')

		{{-- start of requests section --}}
		<div class="row">
			<div class="col-12 col-md-8">
				
				@include('transactions.partials.main-accordion')
				
				
			</div>
		</div>
		{{-- end of requests section --}}

	</div>

@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myList h2").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endif