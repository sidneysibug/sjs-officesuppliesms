 <div class="card">
        <div class="card-header" id="headingOneAccordion">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
              {{$transaction->request_code}}
              <span class="badge badge-success">
                  {{ $transaction->status->name}} 
                  
              </span>
            </button>
          </h2>
        </div>

        <div id="collapse{{$transaction->id}}" class="collapse" aria-labelledby="headingOneAccordion" data-parent="#accordionExample">
          <div class="card-body">
            
            {{-- @include('transactions.partials.summary') --}}
            <a href="{{route('transactions.show', $transaction->id)}}">
              View details
            </a>
          </div>
        </div>
</div>