<form action="{{route('transactions.update',$transaction->id)}}" 
	method="post" 
	class=" border">
	@csrf
	@method("PUT")
	<label for="status_id">Edit:</label>
	<select name="status_id" id="status_id" class="form-control form-control-sm">
		@foreach($statuses as $status)
			<option value="{{$status->id}}">{{$status->name}}</option>
		@endforeach
	</select>
	<button class="btn btn-sm btn-outline-primary my-1">Edit Status</button>
</form>

{{-- <form action="{{route('transactions.update',$transaction->id)}}" 
	method="post" 
	class=" border">
	@csrf
	@method("PUT")
	<label for="status_id">Edit:</label>
	<select name="status_id" id="status_id" class="form-control form-control-sm">
		<option value="Pending"{{ $transaction->status_id == 1 ? 'selected' : ''}}>Pending</option>
		<option value="Approved"{{ $transaction->status_id == 2 ? 'selected' : ''}}>Approved</option>
		<option value="Completed"{{ $transaction->status_id == 4 ? 'selected' : ''}}>Completed</option>
		<option value="Declined"{{ $transaction->status_id == 3 ? 'selected' : ''}}>Declined</option>
	</select>
	<button class="btn btn-sm btn-outline-primary my-1">Edit Status</button>
</form> --}}


