<div class="accordion" id="accordionExample">

@cannot('isAdmin')
  @foreach($transactions as $transaction)
    @if($transaction->user_id === Auth::user()->id)
      <?php $status = App\Status::find($transaction->status_id)?>
      <?php $user = App\User::find($transaction->user_id)?>
      {{-- transaction card start --}}
      <div class="card" id="myList">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
              {{$transaction->request_code}}
              {{-- <span class="badge badge-{{ $status->id == 1 ? 'success' : ($status->id == 2 ? 'warning' : 'danger')}}">
                  {{ $status->name}}  
              </span> --}}
              @if($status->id == 1)
                <span class="badge badge-success">{{ $status->name}}</span>
              @elseif($status->id == 2)
                <span class="badge badge-primary">{{ $status->name}}</span>
              @endif

              @if($status->id == 3)
                <span class="badge badge-danger">{{ $status->name}}</span>
              @elseif($status->id == 4)
                <span class="badge badge-info">{{ $status->name}}</span>
              @endif
            </button>
          </h2>
        </div>

        <div id="collapse{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
            
            @include('transactions.partials.summary')
            <a href="{{route('transactions.show', $transaction->id)}}">View details</a>
          </div>
        </div>
      </div>
      {{-- transaction card start --}}
      @endif
  @endforeach
@endcannot


@can('isAdmin')
  @foreach($transactions as $transaction)
      <?php $status = App\Status::find($transaction->status_id)?>
      <?php $user = App\User::find($transaction->user_id)?>
      {{-- transaction card start --}}
      <div class="card" id="myList">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
              {{$transaction->request_code}}
              {{-- <span class="badge badge-{{ $status->id == 1 ? 'success' : ($status->id == 2 ? 'warning' : 'danger')}}">
                  {{ $status->name}}  
              </span> --}}
              @if($status->id == 1)
                <span class="badge badge-success">{{ $status->name}}</span>
              @elseif($status->id == 2)
                <span class="badge badge-primary">{{ $status->name}}</span>
              @endif

              @if($status->id == 3)
                <span class="badge badge-danger">{{ $status->name}}</span>
              @elseif($status->id == 4)
                <span class="badge badge-info">{{ $status->name}}</span>
              @endif
            </button>
          </h2>
        </div>

        <div id="collapse{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
            
            @include('transactions.partials.summary')
            <a href="{{route('transactions.show', $transaction->id)}}">View details</a>
          </div>
        </div>
      </div>
      {{-- transaction card start --}}

  @endforeach
@endcan

</div>

