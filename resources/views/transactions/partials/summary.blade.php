<div class="table-responsive">

	<table class="table table-hover">
		{{-- transacation code --}}
		<tr>
			<td>Reference number:</td>
			<td>{{$transaction->request_code}}</td>
		</tr>
		{{-- transaction code end --}}
		
		{{-- Customer name start --}}
		<tr>
			<td>Employee's name:</td>
			<td>{{ $transaction->user->name }}</td>
		</tr>
		{{-- Customer name end --}}

		{{-- Date start --}}
		<tr>
			<td>Date requested:</td>
			<td>{{ date("M d, Y - g:i A", strtotime($transaction->created_at)) }}</td>
		</tr>
		{{-- Date end --}}


		{{-- Status start --}}
		<tr>
			<td>Status</td>
			<td>{{ $transaction->status->name}}</td>
			@can('isAdmin')
				<td>
					@include('transactions.partials.edit-status')
				</td>
			@endcan
		</tr>
		{{-- Status end --}}

		{{-- start of reason --}}
		<tr>
			<td>Reason for the request:</td>
			<td>{{ $transaction->reason}}</td>
		</tr>
		{{-- end of reason --}}

		{{-- borrow_date start --}}
		<tr>
			<td>Date needed:</td>
			<td>{{ date("M d, Y", strtotime($transaction->date_needed)) }}</td>
		</tr>
		{{-- borrow_date end --}}

		{{-- borrow_date start --}}
		<tr>
			<td>Date return:</td>
			<td>{{ date("M d, Y", strtotime($transaction->date_return)) }}</td>
		</tr>
		{{-- borrow_date end --}}

		{{-- start of requested unit --}}
		<tfoot>
			<tr>
				<th>Requested Unit:</th>
			</tr>

			@foreach($transaction->units as $unit)
			<tr>
				{{-- <?php $unit = App\Status::find($transaction->unit_id)?> --}}
				{{-- <?php $unit = DB::table('transaction_unit')->where('unit_id', $unit->id)?> --}}

				<td>{{ $unit->unit_code}}</td>
				<td>{{ $unit->availability->name}}</td>

			</tr>
			@endforeach
		</tfoot>
		{{-- end of requested unit --}}

	</table>
</div>
{{-- table end --}}