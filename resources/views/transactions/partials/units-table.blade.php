<div class="row">
	<div class="col-12">
		{{-- table start --}}
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Asset Code</th>
						<th>Asset Category</th>
						<th>Particulars</th>
						<th>UOM</th>
					</tr>
				</thead>
				<tbody>
					@foreach($transaction->units as $unit)
						<tr>
							<td>{{ $unit->unit_code}}</td>
							<td>{{ $unit->category->name}}</td>
							<td>{{ $unit->particulars}}</td>
							<td>{{ $unit->uom}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		{{-- table end --}}
	</div>
</div>
