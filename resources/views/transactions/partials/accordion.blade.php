
<a 
  id="list-status1-list"
  data-toggle="list"
  href="#list-status1"
  role="tab"
  aria-controls="status1"
  class="list-group-item justify-content-between align-items-left list-group-item-action"
>
  Pending
</a>

<a 
  id="list-status2-list"
  data-toggle="list"
  href="#list-status2"
  role="tab"
  aria-controls="status2"
  class="list-group-item justify-content-between align-items-left list-group-item-action"
>
  Approved
</a>

<a 
  id="list-status3-list"
  data-toggle="list"
  href="#list-status3"
  role="tab"
  aria-controls="status3"
  class="list-group-item justify-content-between align-items-left list-group-item-action"
>
  Declined
</a>

<a 
  id="list-status4-list"
  data-toggle="list"
  href="#list-status4"
  role="tab"
  aria-controls="status4"
  class="list-group-item justify-content-between align-items-left list-group-item-action"
>
  Completed
</a>




<div 
  class="tab-pane fade show"
  id="list-status1"
  role="tabpanel"
  aria-labelledby="list-status1-list" 
>
   @include('transactions.partials.status1')
</div>

<div 
  class="tab-pane fade show"
  id="list-status2"
  role="tabpanel"
  aria-labelledby="list-status2-list" 
>
   @include('transactions.partials.status2')
</div>

<div 
  class="tab-pane fade show"
  id="list-status3"
  role="tabpanel"
  aria-labelledby="list-status3-list" 
>
   @include('transactions.partials.status3')
</div>


<div 
  class="tab-pane fade show"
  id="list-status4"
  role="tabpanel"
  aria-labelledby="list-status4-list" 
>
   @include('transactions.partials.status4')
</div>