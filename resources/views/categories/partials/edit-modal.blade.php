<button 
  id="editModalBtn" 
  type="button" 
  class="btn btn-warning" 
  data-toggle="modal" 
  data-target="#exampleModalEdit{{ $category->id}}" 
  data-whatever="Edit">
  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"/>
  <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"/>
  </svg>
  Edit
</button>


<div class="modal fade" id="exampleModalEdit{{ $category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel{{ $category->id}}" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel{{ $category->id}}">Edit Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- start of form --}}
        <form action="{{ route('categories.update', $category->id)}}" method="post" class="mt-2">
          @csrf
          @method('PUT')
          

          <label for="name">Asset category:</label>
          <input 
            type="text" 
            name="name" 
            id="name"
            class="form-control form-control-sm" 
            value="{{ $category->name}}"
          >
          <button class="btn btn-sm btn-warning mt-1">Save changes</button>
        </form>
        {{-- end of form --}}


      </div>
    </div>
  </div>
</div>
