@if(Auth::user())
@extends('layouts.app')

@section('content')
	
	<div class="container">

		<div class="row mb-3">
			@can('isAdmin')
				<div class="mt-4 mb-2 col-12">	
					@include('categories.partials.create-modal')	
				</div>
				

				<div class="col-12 col-md-4">
					@include('categories.partials.admin')
				</div>
			@endcan	
		</div>
		<div class="row">
			<div class="col-12 col-md-6">
				@include('categories.partials.header')
			</div>
		</div>
		<hr>

		{{-- start alert --}}
		{{-- alert-message --}}
		@includeWhen(Session::has('message'),'partials.alert')
		@error('name')
            <div class="row">
				<div class="col-12 alert alert-danger">
					{{ $message }}
				</div>
			</div>
        @enderror

        @error('particulars')
            <div class="row">
				<div class="col-12 alert alert-danger">
					{{ $message }}
				</div>
			</div>
        @enderror

         @error('uom')
            <div class="row">
				<div class="col-12 alert alert-danger">
					{{ $message }}
				</div>
			</div>
        @enderror
         @error('image')
            <div class="row">
				<div class="col-12 alert alert-danger">
					{{ $message }}
				</div>
			</div>
        @enderror


		<div class="row">
			@include('categories.partials.card', ['view' => false])
		</div>

		{{-- start of units display --}}
		<div class="row mt-3">
			@foreach($units as $unit)
				@if($category->id === $unit->category_id)
				<div class="col-12 col-md-4 mx-auto">
					
						{{-- start of unit card --}}
						<div class="card">
							<a 
								href="{{ route('units.show', $unit->id)}}">
								{{-- {{dd(asset('/images/units'))}} --}}
								<img src="{{ secure_asset('/images/units/' . $unit->image) }}" alt="" class="card-img-top"> 
							</a>
							<div class="card-body">
								
								{{-- start of unit-code --}}
								<p class="card-text">
									<strong>
									Asset code: {{ strtoupper($unit->unit_code)}}
									</strong>
								</p>
								{{-- end of unit_code --}}

								<p class="card-text">
							
								
								<small class="d-block">
									Particulars: {{ $unit->particulars}}
								</small>
								<small class="d-block">
									UOM: {{ $unit->uom}}
								</small>
								</p>

								<p class="card-text">
									<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-tag mr-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
	                                  <path fill-rule="evenodd" d="M2 2v4.586l7 7L13.586 9l-7-7H2zM1 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 1 6.586V2z"/>
	                                  <path fill-rule="evenodd" d="M4.5 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm0 1a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
	                                </svg>
									{{ $unit->category->name}}
								</p>


								{{-- start of unit status --}}
								<p class="card-text">
										
										<span class="badge badge-{{ $unit->availability_id == 1 ? 'success' : ($unit->availability_id == 2 ? 'warning' : 'danger')}}">
										{{ $unit->availability->name}}	
										</span>
								</p>
								{{-- end of unit status --}}



								{{-- start of unit category, particulars, and UOM --}}
								{{-- <button
									class="btn btn-sm border-none" 
									type="button" 
									data-toggle="collapse" 
									data-target="#collapseExample{{ $unit->unit_code}}" 
									aria-expanded="false" 
									aria-controls="collapseExample{{ $unit->unit_code}}">
								    Details
								 </button>

								 <div class="collapse" id="collapseExample{{ $unit->unit_code}}">
									  <div class="card card-body">
									  	<p class="card-text">
									  		Category: {{ $unit->category->name}}
									  	</p>
									    <p class="card-text"> 
												Particulars: {{ $unit->particulars}}
												<small class="d-block">
												UOM: {{ $unit->uom}}
												</small>
											</p>
									  </div>
									</div> --}}
								{{-- end of unit category, particulars, and UOM --}}


								

								{{-- start of request --}}
								@cannot('isAdmin')
										@include('units.partials.request-form')
								@endcannot
								{{-- end of request --}}

								{{-- start of view btn --}}
								@if(!isset($view))
									<a href="{{ route('units.show', $unit->id)}}" class="btn btn-sm viewBtn w-100 mt-1">
									<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						                <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
						                <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
						              </svg>
										View
										</a>
								@endif
								{{-- end of view btn --}}
								
								@can('isAdmin')
									{{-- start of edit btn --}}
									@include('units.partials.edit-btn')
									{{-- end of edit btn --}}

									{{-- start of delete btn --}}
									@include('units.partials.delete-form')
									{{-- end of delete btn --}}
								@endcan

							</div>

						</div>
						{{-- end of unit card --}}
				</div>
				@endif
			@endforeach
		</div>
		{{-- end of units display --}}

	</div>
@endsection
@endif
