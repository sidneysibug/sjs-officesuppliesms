@if(Auth::user())
@extends('layouts.app')

@section('content')
	<div class="container">
<div class="row row-cols-1 row-cols-md-2 mt-4">

  <div class="col mb-4">
    <div class="card">
      
      <div class="card-body">
        <h5 class="card-title">
        	<svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-bar-chart-steps mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M.5 0a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-1 0V.5A.5.5 0 0 1 .5 0z"/>
			  <rect width="5" height="2" x="2" y="1" rx=".5"/>
			  <rect width="8" height="2" x="4" y="5" rx=".5"/>
			  <path d="M6 9.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-6a.5.5 0 0 1-.5-.5v-1zm2 4a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z"/>
			</svg>
			Steps in making a request
        </h5>
        <p class="card-text">
        	1. Click on the 'Assets' and search for the assets you need. 
        </p>

        <p class="card-text">
        	2. Create a request by clicking the Request button.
        </p>
        <p class="card-text">
        	3. Once submitted, your request will be processed within the next forty-eight (48) hours.
        </p>
        <p class="card-text">
        	4. If the request has been approved, kindly wait for the Admin to distribute the requested asset/s.
        </p>
        <p class="card-text">
        	5. If you are in great need of an asset and it is unavailable, under maintenace, and/or not in the assets catalog, kindly contact the Admin through the following details:
        </p>
        <p class="card-text">
        	<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-envelope mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
			</svg>
			admin@email.com
        </p>

        <p class="card-text">
        	<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-telephone mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M3.925 1.745a.636.636 0 0 0-.951-.059l-.97.97c-.453.453-.62 1.095-.421 1.658A16.47 16.47 0 0 0 5.49 10.51a16.471 16.471 0 0 0 6.196 3.907c.563.198 1.205.032 1.658-.421l.97-.97a.636.636 0 0 0-.06-.951l-2.162-1.682a.636.636 0 0 0-.544-.115l-2.052.513a1.636 1.636 0 0 1-1.554-.43L5.64 8.058a1.636 1.636 0 0 1-.43-1.554l.513-2.052a.636.636 0 0 0-.115-.544L3.925 1.745zM2.267.98a1.636 1.636 0 0 1 2.448.153l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
			</svg>
			(02) 898 7777
        </p>
      </div>
    </div>
  </div>

    <div class="col mb-4">
    <div class="card">
      
      <div class="card-body">

        <h5 class="card-title">
        	<svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-calendar mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			  <path fill-rule="evenodd" d="M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1zm1-3a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z"/>
			  <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5zm9 0a.5.5 0 0 1 .5.5V1a.5.5 0 0 1-1 0V.5a.5.5 0 0 1 .5-.5z"/>
			</svg>
			Date Definition
       </h5>
        <p class="card-text">
        	<strong>Date needed:</strong>
        </p>
        <p class="card-text">
        	Refers to the date when you'll be needing the asset/s. Must be atleast two (2) days from the date requested. 
        </p>

        <p class="card-text">
        	<strong>Date return:</strong>
        </p>
        <p class="card-text">
        	Refers to the date when you'll be returning the asset/s. Must be within the next thirty (30) days from the date needed.
        </p>
      </div>
    </div>
  </div>

</div>
	</div>
@endsection
@endif