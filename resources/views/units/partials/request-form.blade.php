<form action="{{route('requestform.update', $unit->id) }}" method="post">	
	@csrf
	@method('PUT')
	
	<button 
		class="
		btn 
		btn-sm
		w-100 mt-1
		btn-{{ $unit->availability_id !== 1 ? "danger" : "success"}}"
		{{ $unit->availability_id !== 1 ? "disabled" : ""}}
	>
		{{ $unit->availability_id !== 1 ? "Unavailable" : "Request"}}
		
	</button>
</form>