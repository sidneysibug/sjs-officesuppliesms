
<div class="my-1">
    <p> 
      <a 
      class="btn btn-success" 
      data-toggle="collapse" 
      href="#collapseExampleAddAsset" 
      role="button" 
      aria-expanded="false" 
      aria-controls="collapseExampleAddAsset">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z"/>
              <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z"/>
            </svg>
             Add Asset
      </a>
    </p>
    <div 
      class="collapse" 
      id="collapseExampleAddAsset">
      <div class="card card-body">
        
          <form 
            action="{{ route('units.store')}}"
            method="post" 
            enctype="multipart/form-data" 
          >
            @csrf

            {{-- unit_code --}}
            <label for="unit_code">Unit Code:</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="">
                  _cat_id_
                </span>
              </div>
              
              <input 
                type="text" 
                name="unit_code" 
                id="unit_code" 
                class="form-control" 
                value="" 
                placeholder=""
                 
                >
            </div>
            @error('unit_code')
            <small class="d-block invalid-feedback">
              <strong>
                {{ $message }}
              </strong>
            </small>
            @enderror

            {{-- start category_id --}}
            <label for="category_id" class="mt-1">Asset type:</label>
            <select name="category_id" id="category_id" class="form-control form-control-sm">
              @foreach($categories as $category)
                <option value="{{ $category->id}}">{{ $category->name }}</option>
              @endforeach
              
            </select>
            @error('category_id')
              <small class="d-block invalid-feedback">
                <strong>              
                  {{ $message }}
                </strong>
              </small>
            @enderror
            {{-- end category_id --}}

            {{-- particulars --}}
            @include('units.partials.form-group',[
              'name' => 'particulars',
              'type' => 'text',
              'classes' => ['form-control', 'form-control-sm']
            ])


            {{-- UOM --}}
            @include('units.partials.form-group',[
              'name' => 'uom',
              'type' => 'text',
              'classes' => ['form-control', 'form-control-sm']
            ])

            {{-- image --}}
            @include('units.partials.form-group',[
              'name' => 'image',
              'type' => 'file',
              'classes' => ['form-control-file', 'form-control-sm', 'text-center', 'pl-0']
            ])

            <button class="btn btn-sm btn-warning mt-1">Add Unit</button>

          </form>

      </div>
    </div>
</div>

