@if(Auth::user())
@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<h1 class="">Create Unit</h1>
			</div>
		</div>

		{{-- edit form start --}}
		<div class="row">
			<div class="col-12 col-md-6 ">
				<div class="card p-2">
					
					<form 
						action="{{ route('units.store')}}"
						method="post" 
						enctype="multipart/form-data" 
					>
						@csrf

						{{-- unit_code --}}
						<label for="unit_code">Unit Code:</label>
						<div class="input-group">
							<div class="input-group-prepend">
						    <span class="input-group-text" id="">
						    	_cat_id_
						    </span>
						  </div>
							
							<input 
								type="text" 
								name="unit_code" 
								id="unit_code" 
								class="form-control" 
								value="{{ $unit_code}}" 
								placeholder=""
								 
								>
						</div>
						@error('unit_code')
						<small class="d-block invalid-feedback">
							<strong>
								{{ $message }}
							</strong>
						</small>
						@enderror

						{{-- start category_id --}}
						<label for="category_id" class="mt-1">Asset type:</label>
						<select name="category_id" id="category_id" class="form-control form-control-sm">
							@foreach($categories as $category)
								<option value="{{ $category->id}}">{{ $category->name }}</option>
							@endforeach
							
						</select>
						@error('category_id')
							<small class="d-block invalid-feedback">
								<strong>							
									{{ $message }}
								</strong>
							</small>
						@enderror
						{{-- end category_id --}}

						{{-- particulars --}}
						@include('units.partials.form-group',[
							'name' => 'particulars',
							'type' => 'text',
							'classes' => ['form-control', 'form-control-sm']
						])


						{{-- UOM --}}
						@include('units.partials.form-group',[
							'name' => 'uom',
							'type' => 'text',
							'classes' => ['form-control', 'form-control-sm']
						])

						{{-- image --}}
						@include('units.partials.form-group',[
							'name' => 'image',
							'type' => 'file',
							'classes' => ['form-control-file', 'form-control-sm', 'text-center', 'pl-0']
						])

						<button class="btn btn-sm btn-warning mt-1">Add Unit</button>

					</form>
				</div>
			</div>
		</div>
		{{-- edit form end --}}
	</div>

@endsection
@endif