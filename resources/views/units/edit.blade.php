@if(Auth::user())
@extends('layouts.app')
@section('content')

	<div class="container">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="Edit">Edit Unit</button>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Asset Unit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form 
            action="{{ route('units.update', $unit->id)}}"
            method="post" 
            enctype="multipart/form-data" 
          >
            @csrf
            @method('PUT')

            {{-- unit_code --}}
            <label for="unit_code">Unit Code:</label>
            <div class="input-group">
              <input 
                type="text" 
                name="unit_code" 
                id="unit_code" 
                class="form-control" 
                value="{{ $unit->unit_code}}" 
                readonly
                >
            </div>

            <div class="form-group">
              <label for="image" class="col-form-label">Change Image:</label>
              <input type="file" class="form-control" id="image" name="image">
            </div>

            <label for="availability_id" class="mt-1">Asset type:</label>
            <select name="availability_id" id="availability_id" class="form-control form-control-sm">
              @foreach($availabilities as $availability)
                <option value="{{ $availability->id}}">{{ $availability->name }}</option>
              @endforeach
              
            </select>

            <button class="btn btn-sm btn-warning mt-1">Save Changes</button>

          </form>
      </div>

    </div>
  </div>
</div>

	</div>

@endsection
@endif