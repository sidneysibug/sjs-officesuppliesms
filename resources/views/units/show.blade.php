@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8">
				@include('units.partials.header')
			</div>
		</div> 
		<hr>

		{{-- alert-message --}}
		@includeWhen(Session::has('message'),'partials.alert')

		<div class="row">
			<div class="col-md-4">
				<div class="card">

					{{-- <img src="{{ $unit->image}}" alt="" class="card-img-top"> --}}
					{{-- <img src="{{ asset('images/units/' . $unit->image) }}" alt="" class="card-img-top"> --}}
					{{-- {{dd(asset('/images/units'))}}
					<img src="{{ asset('/images/units/' . $unit->image) }}" alt="" class="card-img-top"> --}}
					{{-- {{dd(asset('/images/units'))}} --}}
					{{-- <img src="{{'http://localhost:8000/images/units/' . $unit->image) }}" alt="" class="card-img-top">  --}}

					{{-- <img src="{{ asset($unit->image) }}" alt="" class="card-img-top">  --}}

					<img src="{{ secure_asset('/images/units/' . $unit->image) }}" alt="" class="card-img-top"> 

				</div>
			</div>

			<div class="col-md-4">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							Asset code: {{ $unit->unit_code}}
						</h5>
						{{-- categor name --}}
						<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-tag mr-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path fill-rule="evenodd" d="M2 2v4.586l7 7L13.586 9l-7-7H2zM1 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 1 6.586V2z"/>
                                  <path fill-rule="evenodd" d="M4.5 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm0 1a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
                        </svg>
                        {{ $unit->category->name}}
						{{-- end of category name --}}
						<p class="card-text">
							<span class="badge badge-{{ $unit->availability_id == 1 ? 'success' : ($unit->availability_id == 2 ? 'warning' : 'danger')}}">
										{{ $unit->availability->name}}	
							</span>
						</p>
						<hr>
						<p class="card-text">
							Particulars: {{ $unit->particulars}}
						</p>
						<p class="card-text">
							UOM: {{ $unit->uom}}
						</p>
					</div>
					<div class="card-footer">
						@cannot('isAdmin')
						{{-- start of request --}}
							@include('units.partials.request-form')
						{{-- end of request --}}
						@endcannot

						@can('isAdmin')
						{{-- start of edit btn --}}
							@include('units.partials.edit-btn')
						{{-- end of edit btn --}}

						{{-- start of delete btn --}}
							@include('units.partials.delete-form')
						{{-- end of delete btn --}}
						@endcan
					</div>

				</div>
			</div>
		</div>
		
	</div>
@endsection
